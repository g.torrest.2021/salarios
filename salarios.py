#funciones

#programa principal
import unittest


tipo_impuesto=30
class Empleado:
    """un ejemplo de clase para empleados"""
    def __init__(self,n,s):
        self.nombre=n
        self.salario=s
    def aplicador_impuesto(self):
        porcentaje=30/100
        return self.salario*porcentaje
    def __str__(self):
        return "el empleado {nom} debe pagar  {tax:.2f} euros".format(nom=self.nombre,tax=self.aplicador_impuesto())
    def calculador_coste_salarial(self):
        impuesto=self.aplicador_impuesto()
        coste=self.salario+impuesto
        return coste
class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1=Empleado("nombre",5000)
        self.assertEqual(e1.nomina,5000)
    
    def test_impuestos(self):
        e1=Empleado("nombre",5000)
        self.assertEqual(e1.aplicador_impuesto,5000)

    def test_str(self):
        e1=Empleado("nombre",5000)
        self.assertEqual("El empleado pepe debe pagar 15000.00",e1.__str__())
empleadoPepe=Empleado("pepe",20000)
empleadoAna=Empleado("ana",30000)
impuesto=empleadoPepe.aplicador_impuesto()
impuesto_2=empleadoAna.aplicador_impuesto()
ana=empleadoAna.__str__()
print(ana)
print(empleadoAna.calculador_coste_salarial())
if __name__=="__main__":
    unittest.main()