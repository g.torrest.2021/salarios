import unittest
from empleado import Empleados

class TestEmpleado(unittest.TestCase):

    def test_construir(self):
        e1 = Empleados("nombre", 5000)
        self.assertEqual(e1.nomina, 5000)
         
    def test_impuestos(self):
        e1 = Empleados("nombre", 5000)
        self.assertEqual(e1.CalculoImpuestos(), 1500.0)

    def test_str(self):
        e1 = Empleados("pepe", 50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())

if __name__ == "__main__":
    unittest.main()

