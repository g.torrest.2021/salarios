class Empleados:
    """Los metodos se suelen poner en minuscula"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def CalculoImpuestos(self):
        return self.nomina * 0.3

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.CalculoImpuestos())
class Jefes(Empleados):
    def __init__(self, n, s,p):
        super().__init__(n, s)
        self.prima=p
    def CalculoImpuestos(self):
        sal_pri=self.nomina+self.prima
        impuesto=sal_pri*0.3
        return impuesto
    def CalculoImpuestos2(self):
        return super().CalculoImpuestos(self)+self.prima*0.3
    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name=self.nombre,tax=self.CalculoImpuestos())
jefe=Jefes("Juan",45000,700)
print(jefe.CalculoImpuestos())
print(jefe.CalculoImpuestos2)
print(jefe)